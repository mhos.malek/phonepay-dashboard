import React, { Component } from 'react';
import {RouterController} from "../Router"
import {Provider} from "react-redux"
import configureStore from '../Store/configureStore';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RouterController />
     </Provider>
    );
  }
}

export default App;
