import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";

//private Routes
import { ConnectedHome } from "../Views/Pages/Home";
import { Profile } from "../Views/Pages/Profile";
import { MerchantsList } from "../Views/Pages/MerchantsList";
import { PartnersList } from "../Views/Pages/PartnersList";
import { UsersList } from "../Views/Pages/UsersList";
import { UsersGroup } from "../Views/Pages/UsersGroup";

//publicRoutes
import {LoginPage} from "../Views/Pages/Login";
import {About} from "../Views/Pages/About"
import {NotFound} from "../Views/Pages/NotFound";

//private and public routes component
import {PrivateRoute} from "./PrivateRoute"
import { PublicRoute } from "./PublicRoute";

const RouterController = () => {
  return (
    <div>
      <Router>
          <Switch>

            <PublicRoute exact path="/login" component={LoginPage}/>
            <PublicRoute exact path="/about" component={About}/>

            <PrivateRoute exact path="/" component={ConnectedHome}/>
            <PrivateRoute exact path="/profile" component={Profile}/>
            <PrivateRoute exact path="/merchants" component={MerchantsList}/>
            <PrivateRoute exact path="/partners" component={PartnersList}/>
            <PrivateRoute exact path="/users-group" component={UsersGroup}/>
            <PrivateRoute exact path="/users" component={UsersList}/>

            <PublicRoute exact path="*" component={NotFound} /> 

          </Switch>
      </Router>
    </div>
  );
};
export { RouterController };
