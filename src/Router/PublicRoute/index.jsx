import React from "react";
import { Route } from "react-router-dom";
import { AuthLayout } from "../../Views/Layouts/AuthLayout";
const PublicRoute = props => {
  return (
    <AuthLayout>
      <Route {...props} />
    </AuthLayout>
  );
};
export { PublicRoute };
