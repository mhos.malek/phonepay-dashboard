import React from "react";
import { Route } from "react-router-dom";
import {PanelLayout} from "../../Views/Layouts/PanelLayout";

const PrivateRoute = props => {
  return (
    <PanelLayout>
      <Route {...props} />
    </PanelLayout>
  );
};
export { PrivateRoute };
