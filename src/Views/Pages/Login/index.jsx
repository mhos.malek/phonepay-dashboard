import React from "react";
import { LoginForm } from "./Components/LoginForm";
import { Card } from 'antd';

const LoginPage = () => {
  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height:'100vh'
    }}>
      <Card style={{ width: 300 }}>
        <LoginForm />
      </Card>
    </div>
  );
};
export { LoginPage };
