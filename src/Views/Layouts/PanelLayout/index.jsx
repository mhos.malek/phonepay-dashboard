import React from "react";
import { Layout, Menu, Icon } from "antd";
import {  Link } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;

const PanelLayout = props => {
  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["4"]}>
          <Menu.Item key="1">
          <Link to="/profile" > 
            <Icon type="user" />
            <span className="nav-text">
                profile
            </span>
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
          <Link to="/users" > 
            <Icon type="video-camera" />
            <span className="nav-text">users</span>
          </Link>
          </Menu.Item>

          <Menu.Item key="3">
            <Icon type="upload" />
            <span className="nav-text">nav 3</span>
          </Menu.Item>
          <Menu.Item key="4">
            <Icon type="user" />
            <span className="nav-text">nav 4</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: "#fff", padding: 0 }} />
        <Content style={{ margin: "24px 16px 0" }}>
          <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
            {props.children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          malek@phonepay.ir
        </Footer>
      </Layout>
    </Layout>
  );
};
export { PanelLayout };
