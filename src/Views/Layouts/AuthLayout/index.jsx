import React from "react";
const AuthLayout = (props) => {
  return (
    <div
      style={{
        height: "100vh",
        overflowY: 'hidden',
        backgroundColor: '#f8f8f8',
      }}
    >
       {props.children}
    </div>
  );
};

export { AuthLayout };
