import {
  ADD_TODO_SUCCESS,
  ADD_TODO_FAILURE,
} from './actionTypes';

import axios from 'axios';

export const addTodo = ({ title, userId }) => {
  return dispatch => {
  axios
      .post(`https://jsonplaceholder.typicode.com/todos`, {
        title,
        userId,
        completed: false
      })
      .then(res => {
        dispatch(signInSuccess(res.data));
      })
      .catch(err => {
        dispatch(signInFailure(err.message));
      });
  };
};

export const signInAction = ({ email, password }) => {
  return dispatch => {
  axios
      .post(`http://192.168.40.243:18980/api/v1.0/business/signin`, {
        email,
        password,
      })
      .then(res => {
        console.log(res)
        dispatch(signInSuccess(res.data));
      })
      .catch(err => {
        dispatch(signInFailure(err.message));
      });
  };
};

const signInSuccess = todo => ({
  type: ADD_TODO_SUCCESS,
  payload: {
    ...todo
  }
});

const signInFailure = error => ({
  type: ADD_TODO_FAILURE,
  payload: {
    error
  }
});